package com.fpt.service;

import org.springframework.stereotype.Service;

import com.twilio.Twilio;
import com.twilio.exception.TwilioException;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class TwilioMessageCreator {
	public static final String ACCOUNT_SID = "AC931c57e64e0cd0d61a8b386ade2dd2c9";
	public static final String AUTH_TOKEN = "a1c1e2713d11b4dd21ee9b66b7d710b3";
	public static final String TWILIO_NUMBER = "+13344234425";

	public void sendSMS(String msg, String phone) throws TwilioException {

		Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

		Message message = Message.creator(new PhoneNumber("+84" + phone.substring(1)), // to
				new PhoneNumber(TWILIO_NUMBER), // from
				msg).create();

		System.out.println(message.getSid());
	}

}
