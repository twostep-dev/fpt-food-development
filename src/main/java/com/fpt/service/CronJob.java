package com.fpt.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import com.fpt.dao.ReportDao;
import com.fpt.dao.UserDao;
import com.fpt.model.Users;

public class CronJob {

	@Autowired
	UserDao userDaoimpl;

	@Autowired
	ReportDao reportDaoimpl;

//	@Scheduled(cron = "[Seconds] [Minutes] [Hours] [Day of month] [Month] [Day of week] [Year]")

	@Scheduled(cron = "0 0 0 * * ?")
//	@Scheduled(cron = "0/5 * * * * ?")

	public void autoUnlockUser() throws ParseException {

		for (Users user : userDaoimpl.filterByStatus(5)) {
			Date today = new Date();

			SimpleDateFormat fomart = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
			Date block_date = fomart.parse(user.getBlock_date());
			Date unlock_date = new Date(block_date.getTime() + (1000 * 60 * 60 * 24));

			if ((fomart.format(today)).equals(fomart.format(unlock_date))) {
				userDaoimpl.setStatus(user.getId(), 1, null);
				reportDaoimpl.deleteMany(user.getId());

			} else {
				System.out.println("nothing to do ...");
			}
		}

	}

//	@Scheduled(cron = "0/5 * * * * ?")
//	public void tess_job_push_5s() throws ParseException {
//		Date today = new Date();
//		System.out.println(
//				"===========================================================================Time Now:" + today);
//	}

}
