package com.fpt.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fpt.model.Comments;
import com.fpt.model.Commons;
import com.fpt.model.Posts;

@Transactional
@Repository

public class CommonDao {

	private final SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

	@SuppressWarnings("unchecked")
	public List<Commons> list() {
		Session session = sessionFactory.openSession();
		List<Commons> list = session.createQuery("From Commons").list();
		return list;
	}


	@SuppressWarnings("unchecked")
	public List<Commons> findByTypeKind(String type, String kind, int ofset, int limit) {
		Session session = sessionFactory.openSession();
		List<Commons> list = session.createQuery("From Commons where type= '" + type + "' and kind= '" + kind + "'")
				.setFirstResult(ofset).setMaxResults(limit).list();
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Posts> filterByType(String type) {
		Session session = sessionFactory.openSession();
		List<Posts> list = session.createQuery("FROM Commons where  type= '" + type + "' ORDER BY created_at desc")
				.setMaxResults(6).list();
		return list;
	}

	public Commons findByID(int id) {
		try {
			Session session = sessionFactory.openSession();
			Commons Commons = (Commons) session.get(Commons.class, id);
			return Commons;
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}

	}

	public Boolean Delete(int id) {
		Session session = sessionFactory.openSession();
		Commons common = (Commons) session.get(Commons.class, id);
		try {
			session.getTransaction().begin();
			session.delete(common);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			System.out.println(e);
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			return false;
		} finally {
			session.close();
		}
	}

	public boolean Update(Commons common) {
		Session session = sessionFactory.openSession();
		try {
			session.getTransaction().begin();
			session.update(common);
			session.getTransaction().commit();

			return true;
		} catch (Exception e) {
			System.out.println(e);
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			return false;
		} finally {
			session.close();
		}
	}

	public boolean Create(Commons common) {
		Session session = sessionFactory.openSession();
		try {
			session.getTransaction().begin();
			session.save(common);
			session.getTransaction().commit();

			return true;
		} catch (Exception e) {
			System.out.println(e);
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			return false;
		} finally {
			session.close();
		}
	}

}
