package com.fpt.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fpt.dao.CommonDao;
import com.fpt.dao.PostDao;
import com.fpt.dao.UserDao;
import com.fpt.model.Posts;
import com.fpt.model.Reactions;
import com.fpt.model.Users;
import com.fpt.service.CurrentUser;
import com.fpt.service.UploadConfig;

@Controller
public class StaticController {

	@Autowired
	UserDao userDaoimpl;

	@Autowired
	PostDao postDaoimpl;

	@Autowired
	UploadConfig UploadConfig;

	@Autowired
	ServletContext context;
	
	@Autowired
	CommonDao commonDaoimpl;

	@Autowired
	CurrentUser user;

	@GetMapping(value = "/")
	public String index(Model model, HttpSession session) {
		try {
			session.setAttribute("user", user.current());
			model.addAttribute("slides", postDaoimpl.filterHomePage("created_at"));
			model.addAttribute("suggestions", postDaoimpl.filterHomePage("view_conter"));
			return "static/index";
		} catch (Exception e) {
			return "auth/500";
		}
	}

	@GetMapping(value = "/menu")
	public String list(ModelMap model) {
		try {
			return "static/menu";
		} catch (Exception e) {
			return "auth/500";
		}
	}
	
	


	@GetMapping(value = "/contact")
	public String contact(ModelMap model) {

		try {
			return "static/contact";
		} catch (Exception e) {
			return "auth/500";
		}
	}

	@GetMapping(value = "/profile")
	public String profile(ModelMap model) {

		try {
			if (user.exist()) {
				return "static/profile";
			} else {
				return "auth/sign-in";
			}
		} catch (Exception e) {
			return "auth/500";
		}
	}

	@PostMapping(value = "/edit-profile")
	public String profileEditProcess(HttpSession session, ModelMap model, @RequestParam("image") MultipartFile image,
			@RequestParam("gender") int gender, @RequestParam("age") int age, @RequestParam("name") String name,
			@RequestParam("birthday") String birthday, @RequestParam("address") String address,
			@RequestParam("bio") String bio) {
		String photo = UploadConfig.uploadImage(model, image);
		String photo2 = photo.equals("") ? user.current().getImage() : photo;
		try {
			if (userDaoimpl.Update(new Users(user.current().getId(), user.current().getEmail(), name,
					user.current().getPassword(), user.current().getPhone(), age, gender, user.current().getRole(),
					birthday, address, user.current().getProvider(), bio, photo2, user.current().getCreated_at(),
					user.current().getStatus(), user.current().getBlock_date()))) {
				model.addAttribute("message2", "Cập nhập thông tin thành công.");
				model.addAttribute("class_name", "msg_success");
			} else {
				model.addAttribute("message2", "Cập nhập thông tin thất bại.");
				model.addAttribute("class_name", "msg_success");
			}

			session.setAttribute("user", user.current());
			return "static/profile";
		} catch (Exception e) {
			return "auth/500";

		}

	}

	@PostMapping(value = "/change-password")
	public String changepassword(ModelMap model, @RequestParam("password") String password) {

		try {
			if (userDaoimpl.ChangePassword(user.userID(), userDaoimpl.encode(password), null)) {
				model.addAttribute("message", "Thay đổi mật khẩu thành công. ");
				model.addAttribute("class_name", "msg_success");
				return "static/profile";
			} else {
				model.addAttribute("message", "Thay đổi mật khẩu không thành công, vui lòng thử lại . ");
				model.addAttribute("class_name", "msg_error");
				return "static/profile";
			}
		} catch (Exception e) {
			return "auth/500";
		}

	}


	@GetMapping(value = "filter")
	public String filter(ModelMap model, HttpServletRequest request, @RequestParam("condition") String cond,
			@RequestParam(value = "category", defaultValue = "search") String category,
			@RequestParam(name = "page", defaultValue = "1") int page) {

		int limit = 12;
		int ofset = (limit * page) - limit;
		model.addAttribute("path", request.getRequestURL() + "?condition=" + cond + "&category=" + category);
		model.addAttribute("current_page", page);
		try {
			switch (category) {
			case "search":
				model.addAttribute("title", "Có " + postDaoimpl.seacrhFull(cond, 0, 10000000).size()
						+ " kết quả cho: <span> ``" + cond + " `` </span> ");

				model.addAttribute("posts", postDaoimpl.seacrhFull(cond, ofset, limit));
				model.addAttribute("page_size", page_size(postDaoimpl.seacrhFull(cond, 0, 10000000).size(), limit));
				break;
			case "likes":
				model.addAttribute("title", " Bài viết <span> đã thích  </span> ");
				int begin = ofset ;
				int end = (begin + limit) < mapping().size() ? (begin + limit) : ((mapping().size() - ofset) + begin );
				if (mapping().size() == 0) {
					model.addAttribute("posts", mapping());
					model.addAttribute("page_size", 0);

				} else {
					model.addAttribute("posts", mapping().subList(ofset, end));
					model.addAttribute("page_size", page_size(mapping().size(), limit));
				}

				break;

			case "user_id":
				model.addAttribute("s_user", userDaoimpl.findByID(Integer.parseInt(cond)));
				model.addAttribute("posts", postDaoimpl.pagination("user_id", cond, ofset, limit));
				model.addAttribute("page_size", page_size(userDaoimpl.findByID(Integer.parseInt(cond)).posts.size(), limit));
				break;
			case "level":
				model.addAttribute("title", "Món ăn từ <span> " + cond + " </span> ");
				break;
			case "holiday":
				model.addAttribute("title", "Các món ngày <span> " + cond + " </span> ");
				break;
			case "category":
				model.addAttribute("title", "Các món <span> " + cond + " </span> ");
				break;
			case "kind":
				model.addAttribute("title", "Các món <span> " + cond + " </span> ");
				break;
			case "nation":
				model.addAttribute("title", "Món ăn <span> " + cond + " </span> ");
				break;
			case "suitable":
				model.addAttribute("title", "Món ăn dành cho <span> " + cond + " </span> ");
				break;
			default:
				model.addAttribute("title", cond );
				model.addAttribute("check", true );
			}
			
			if (category.equals("Mẹo hay") || category.equals("Dinh dưỡng") ) {
				model.addAttribute("posts", commonDaoimpl.findByTypeKind(category, cond, ofset, limit));
				model.addAttribute("page_size",
						page_size(commonDaoimpl.findByTypeKind(category, cond, 0, 10000000).size(), limit));
			}
			
			else if (!category.equals("likes") && !category.equals("search") && !category.equals("user_id")) {
				model.addAttribute("posts", postDaoimpl.pagination(category, cond, ofset, limit));
				model.addAttribute("page_size",
						page_size(postDaoimpl.pagination(category, cond, 0, 10000000).size(), limit));
			}

			String page_return =  category.equals("user_id") ? "static/viewer" : "static/filter";
			return page_return;
		} catch (Exception e) {
			return "auth/500";
		}
	}

	@GetMapping(value = "undefined")
	public String handle() {
		return "auth/404";
	}

	public int page_size(int totals, int limit) {
		int page_size = totals / limit + (totals % limit == 0 ? 0 : 1);
		return page_size;
	}

	public List<Posts> mapping() {

		List<Posts> posts = new ArrayList<Posts>();

		for (Reactions rc : userDaoimpl.findByID(user.userID()).reactions) {
			Posts post = new Posts();
			post.setId(rc.getPost().getId());
			post.setVideo_url(rc.getPost().getVideo_url());
			post.setTitle(rc.getPost().getTitle());
			post.setOverview(rc.getPost().getOverview());
			post.setCreated_at(rc.getPost().getCreated_at());
			post.setCategory(rc.getPost().getCategory());
			posts.add(post);
		}

		return posts;
	}

}
