package com.fpt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fpt.dao.CommonDao;
import com.fpt.model.Commons;
import com.fpt.service.CurrentUser;
import com.fpt.service.UploadConfig;

@Controller
public class CommonController {

	@Autowired
	CommonDao commonDaoimpl;

	@Autowired
	CurrentUser user;

	@Autowired
	UploadConfig UploadConfig;
	
	private int ofset = 0;
	private int limit  = 8 ;
	


	@GetMapping(value = "/nutrition")
	public String nutrition(ModelMap model) {
		model.addAttribute("children", commonDaoimpl.findByTypeKind("Dinh Dưỡng", "Dinh dưỡng cho trẻ",ofset,limit));
		model.addAttribute("older", commonDaoimpl.findByTypeKind("Dinh Dưỡng", "Dinh dưỡng cho người cao tuổi",ofset,limit));
		model.addAttribute("sick", commonDaoimpl.findByTypeKind("Dinh Dưỡng", "Chế độ ăn cho người bệnh",ofset,limit));
		return "static/nutri";
	}

	@GetMapping(value = "/tips")
	public String tips(ModelMap model) {
		model.addAttribute("tips1", commonDaoimpl.findByTypeKind("Mẹo hay", "Mẹo hay chế biến nguyên liệu",ofset,limit));
		model.addAttribute("tips2", commonDaoimpl.findByTypeKind("Mẹo hay", "Bí quyết nấu nướng",ofset,limit));
		model.addAttribute("tips3", commonDaoimpl.findByTypeKind("Mẹo hay", "Các thực phẩm kị nhau",ofset,limit));
		return "static/tips";
	}

	@GetMapping(value = "/readmore/{id}")
	public String read(ModelMap model, @PathVariable("id") int id) {
		try {
			model.addAttribute("record", commonDaoimpl.findByID(id));

			if (commonDaoimpl.findByID(id).getType().equals("Mẹo hay")) {
				model.addAttribute("refer", commonDaoimpl.filterByType("Mẹo hay"));
			} else {
				model.addAttribute("refer", commonDaoimpl.filterByType("Dinh dưỡng"));
			}
			
			
			return "static/read";
		} catch (Exception e) {
			return "auth/500";
		}

	}

	@GetMapping(value = "admin/common/edit/{id}")
	public String edit(ModelMap model, @PathVariable("id") int id) {

		try {
			if (user.isAdminOrMod()) {
				model.addAttribute("record", commonDaoimpl.findByID(id));
				return "admin/common/edit";
			} else {
				return "auth/401"; 
			}
		} catch (Exception e) {
			return "auth/500";
		}

	}

	@PostMapping(value = "admin/common/edit/{id}")
	public String editProcess(ModelMap model, @PathVariable("id") int id, @RequestParam("author") String author,
			@RequestParam("title") String title, @RequestParam("content") String content,
			@RequestParam("type") String type, @RequestParam("kind") String kind,
			@RequestParam("image") MultipartFile image) {
		String path = UploadConfig.uploadImage(model, image);
		String path_img = path.equals("") ? commonDaoimpl.findByID(id).getImage() : path;


		try {
			if (user.isAdminOrMod()) {
				if (commonDaoimpl
						.Update(new Commons(id, title, content, type, author, user.timestamp.toString(), kind, path_img,user.current()))) {
					model.addAttribute("msg", "Thao tác thành công.");
					model.addAttribute("class_name", "msg_success");

				} else {
					model.addAttribute("msg", "Thao tác thất bại.");
					model.addAttribute("class_name", "msg_error");

				}
				model.addAttribute("commons", commonDaoimpl.list());
				return "admin/common/list";
			} else {
				return "auth/401";
			}
		} catch (Exception e) {
			return "auth/500";
		}

	}

	@GetMapping(value = "admin/common/create")
	public String create(ModelMap model) {

		if (user.isAdminOrMod()) {
			return "admin/common/create";
		} else {
			return "auth/401";
		}
	}

	@GetMapping(value = "admin/common/list")
	public String list(ModelMap model) {
		if (user.isAdminOrMod()) {
			model.addAttribute("commons", commonDaoimpl.list());
			return "admin/common/list";
		} else {
			return "auth/401";
		}
	}

	@PostMapping(value = "admin/common/create")
	public String created(ModelMap model, @RequestParam("author") String author, @RequestParam("title") String title,
			@RequestParam("content") String content, @RequestParam("type") String type,
			@RequestParam("kind") String kind, @RequestParam("image") MultipartFile image) {

		String path_img = UploadConfig.uploadImage(model, image);

		try {
			if (user.isAdminOrMod()) {
				if (commonDaoimpl
						.Create(new Commons(title, content, type, author, user.timestamp.toString(), kind, path_img, user.current()))) {
					model.addAttribute("msg", "Thao tác thành công.");
					model.addAttribute("class_name", "msg_success");

				} else {
					model.addAttribute("msg", "Thao tác thất bại.");
					model.addAttribute("class_name", "msg_error");

				}
				model.addAttribute("commons", commonDaoimpl.list());
				return "admin/common/list";
			} else {
				return "auth/401";
			}
		} catch (Exception e) {
			return "auth/500";
		}

	}

	@GetMapping(value = "admin/common/delete/{id}")
	public String delete(ModelMap model, @PathVariable("id") int id) {

		try {
			if (user.isAdminOrMod()) {
				if (commonDaoimpl.Delete(id)) {
					model.addAttribute("msg", "Thao tác thành công.");
					model.addAttribute("class_name", "msg_success");
				} else {
					model.addAttribute("msg", "Thao tác thất bại.");
					model.addAttribute("class_name", "msg_error");
				}
				model.addAttribute("commons", commonDaoimpl.list());
				return "admin/common/list";
			} else {
				return "auth/401";
			}
		} catch (Exception e) {
			return "auth/500";
		}

	}

}
