<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en-US">
   <head>
      <title>Food Recipes</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <script
         src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script
         src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
      <link rel="stylesheet"
         href="${pageContext.request.contextPath}/resources/static/style.css"
         type="text/css" media="all" />
      <!-- version 3.4 -->
      <link
         href="${pageContext.request.contextPath}/resources/static/css/bootstrap.min.css"
         rel="stylesheet" type="text/css" />
      <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css"
         rel="stylesheet">
   </head>
   <body style="font-family: cursive;">
      <!-- ============= HEADER STARTS HERE ============== -->
      <div id="header-wrapper" class="clearfix">
         <div id="header" class="clearfix">
            <!-- WEBSITE LOGO -->
            <a class="responsive_logo" href="index.html"><img
               src="${pageContext.request.contextPath}/resources/static/images/logo.png"
               alt="" class="logo" /></a> 
            <a href="index.html">
               <h1 class="sitenametext">Food Recipes</h1>
            </a>
            <a href="index.html"><img class="header-img"
               src="${pageContext.request.contextPath}/resources/static/images/header-image.png"
               alt="Food Recipes" /></a>
         </div>
         <!-- end of header div -->
         <span class="w-pet-border"></span>
         <!-- NAVIGATION BAR STARTS HERE -->
         <div id="nav-wrap">
            <div class="inn-nav clerfix">
               <!-- MAIN NAVIGATION STARTS HERE -->
               <ul id="" class="nav" style="width: 100%;">
                  <li style="background: none;"><a
                     href="${pageContext.request.contextPath}/">Trang chủ </a></li>
                  <li><a href="${pageContext.request.contextPath}/menu">Mục
                     lục</a>
                  </li>
                  <li><a href="${pageContext.request.contextPath}/nutrition">
                     Dinh dưỡng</a>
                  </li>
                  <li><a href="${pageContext.request.contextPath}/tips">Mẹo
                     hay </a>
                  </li>
                  <li><a href="${pageContext.request.contextPath}/contact">Giới
                     thiệu</a>
                  </li>
                  <li><a href="mailto:contact-yummy-food@gmail.com">Liên hệ</a></li>
                  <c:choose>
                     <c:when test="${user == null}">
                        <li class="space_right"><a
                           href="${pageContext.request.contextPath}/authorized/SignIn">Đăng
                           nhập </a>
                        <li class="space_right">
                           <a
                              href="${pageContext.request.contextPath}/authorized/SignUp">Đăng
                           ký </a>
                     </c:when>
                     <c:otherwise>
                     <li class="space_right"><a style="color: rgb(255, 255, 255)">
                     <img class="image_profile"
                        src="${pageContext.request.contextPath }/resources/${user.getImage()}" />
                     ${ user.getFullname().split(" ")[0] }
                     </a>
                     <ul class="sub-menu" style="display: none;">
                     <li style="background: none;"><a
                        href="${pageContext.request.contextPath}/filter?condition=${ user.id }&category=user_id">Cá nhận</a></li>
                     <li><a
                        href="${pageContext.request.contextPath}/filter?condition=${user.email }&category=likes">Bài
                     viết yêu thích</a></li>
                     <c:choose>
                     <c:when test="${ user.getRole() != 1 }">
                     <li><a id="manage"
                        href="${pageContext.request.contextPath}/admin/Dashboard">Manage</a></li>
                     </c:when>
                     <c:otherwise>
                     <li></li>
                     </c:otherwise>
                     </c:choose>
                     <li><a href="${pageContext.request.contextPath}/SignOut">Đăng
                     xuất</a></li>
                     </ul></li>
                     </c:otherwise>
                  </c:choose>
               </ul>
            </div>
         </div>
         <!-- end of nav-wrap -->
         <!-- NAVIGATION BAR ENDS HERE -->
      </div>
      <!-- end of header-wrapper div -->
      <!-- ============= HEADER ENDS HERE ============== -->
      <!-- ============= CONTAINER STARTS HERE ============== -->
      <div class="main-wrap">
      <div id="container">
      <!-- WEBSITE SEARCH STARTS HERE -->
      <div class="top-search  clearfix">
         <h3 class="head-pet">
            <span>Yummy Tìm Kiếm </span>
         </h3>
         <form
            action="${pageContext.request.contextPath}/filter?category=likes&condition"
            id="searchform">
            <p>
               <input type="text" name="condition" id="s" class="field"
                  placeholder="tìm kiếm" /> <input type="submit" id="s-submit"
                  value="" />
            </p>
         </form>
         <p class="statement">
            <span class="fireRed"> Gợi ý :</span> <a>Tiêu đề </a> , <a>Xuất
            xứ</a> , <a>Nguyên liệu </a> , <a>Phương pháp</a> , <a>Ngày lễ -
            kỉ niệm</a> <a href="#">...</a>
            <c:choose>
               <c:when test="${user.getFullname()  == null}">
                  <a href="${pageContext.request.contextPath}/authorized/SignIn"
                     class="readmore"
                     style="background-color: forestgreen; float: right; color: white;">
                  <i class="fa fa-plus" aria-hidden="true"></i> Viết bài
                  </a>
               </c:when>
               <c:otherwise>
                  <a href="${pageContext.request.contextPath}/post-create"
                     class="readmore"
                     style="background-color: forestgreen; float: right; color: white;">
                  <i class="fa fa-plus" aria-hidden="true"></i> Viết bài
                  </a>
               </c:otherwise>
            </c:choose>
         </p>
      </div>