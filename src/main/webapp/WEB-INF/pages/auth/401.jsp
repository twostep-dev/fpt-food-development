<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

<title>Yummy - Error</title>

<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Cabin:400,700"
	rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:900"
	rel="stylesheet">

<!-- Custom stlylesheet -->
<link type="text/css" rel="stylesheet" href="css/style.css" />

<link rel="stylesheet" type="text/css"
href="${pageContext.request.contextPath}/resources/css/error.css">
</head>

<body>

	<div id="notfound">
		<div class="notfound">
			<div class="notfound-404">
				<h3>Oops! Đã  xảy ra lỗi </h3>
				<h1>
					<span>4</span><span>0</span><span>1</span>
				</h1>
			</div>
			<h2>Bạn không đủ quyền truy cập mục này.</h2>
			<a  href="${pageContext.request.contextPath}"class="myButton">Trang chủ</a>
		</div>

	</div>

</body>
<!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
