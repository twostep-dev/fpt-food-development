<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
   <head>
      <title>Đăng nhập</title>
      <meta charset="UTF-8">
      <link rel="stylesheet" type="text/css"
         href="${pageContext.request.contextPath}/resources/css/util.css">
      <link rel="stylesheet" type="text/css"
         href="${pageContext.request.contextPath}/resources/css/main.css">
        <script
         src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   </head>
   <body>
      <div class="limiter">
         <div class="container-login100"
            style="background-image: url('${pageContext.request.contextPath}/resources/images/bg_images_1.jpg');">
            <div class="wrap-login100 p-l-110 p-r-110 p-t-62 p-b-33">
               <span class="login100-form-title p-b-53"> Đăng nhập </span>
               <%-- <button style="float: left" id="btnFacebook" class="btn-face m-b-20">
                  <i class="fa fa-facebook-official"></i> Facebook
                  </button>
                  
                  <button id="btnGoogle" style="margin-left: auto;"
                  class="btn-google m-b-20">
                  <img
                  	src="${pageContext.request.contextPath}/resources/images/icons/icon-google.png"
                  	alt="GOOGLE"> Google
                  </button> --%>
               <form class="login100-form validate-form flex-sb flex-w"
                  action="user-signin" method="post">
                  <div class="p-b-10 p-l-20">
                     <span class="txt1">Số điện thoại </span>
                  </div>
                  <div class="wrap-input100 validate-input"
                     data-validate="Username is required">
                     <input id=username class="input100" type="text" required 
                     	minlength="10" maxlength="50"
                        name="txtphone"> <span class="focus-input100"></span>
                  </div>
                  <div class="p-t-13 p-b-10 p-l-20">
                     <span class="txt1"> Mật khẩu </span> <a
                        href="${pageContext.request.contextPath}/authorized/forgot-password"
                        class="txt2 bo1 m-l-5"> Quên mật khẩu? </a>
                  </div>
                  <div class="wrap-input100 validate-input"
                     data-validate="Password is required">
                     <input class="input100" id="password" minlength="8" maxlength="20" id="password"
                        type="password" required name="txtpass"> <span
                        class="focus-input100"></span>
                  </div>
                  <div style="text-align: center;width: 100%;margin-top: 12px;">
                     <span id="hideMe" class="${class_name}"
                        style="font-family: initial; font-size: larger;">${Msg}</span>
                  </div>
                  
                  <script>
                  
                  setTimeout(function () {
                      $("#hideMe").hide()
                  }, 1000);
                  </script>
                  <div class="container-login100-form-btn m-t-17">
                     <button type="submit" id ="submit" class="login100-form-btn">Đăng nhập
                     </button>
                  </div>
                  <div class="w-full text-center p-t-30">
                     <span class="txt2"> Bạn chưa có tài khoản ? </span> <a
                        href="${pageContext.request.contextPath}/authorized/SignUp"
                        class="txt2 bo1"> Tạo tài khoản mới </a> </br> <a
                        href="${pageContext.request.contextPath}/" class="txt2 bo1">
                     Quay lại trang chủ! </a>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <div id="dropDownSelect1"></div>
   </body>
</html>
