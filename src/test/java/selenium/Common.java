package selenium;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import org.junit.Rule;
import org.junit.rules.TestName;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.ITestResult;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.fpt.dao.CommonDao;
import com.fpt.dao.UserDao;

import connecter.data;

public class Common {
	public static WebDriver driver;

	@Autowired(required = true)
	CommonDao commonDao;

	@Autowired(required = true)
	UserDao userDao;

	@Rule
	public static TestName testName = new TestName();

	@BeforeTest
	public static void setup() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", data.driver_path);
		driver = new ChromeDriver();
		driver.get(data.login_page);
		WebElement usename = driver.findElement(By.id("username"));
		usename.sendKeys(data.username);
		WebElement password = driver.findElement(By.id("password"));
		password.sendKeys(data.password);
		WebElement submit = driver.findElement(By.id("submit"));
		submit.click();
		Thread.sleep(1000);
	}

	@BeforeMethod
	public void afterMethod(ITestResult result) {
		if (result.getMethod().getMethodName().contains("common_validate")) {
			driver.get("http://localhost:8080/FPT-Food_Development/admin/common/create");
		} else {
			driver.get("http://localhost:8080/FPT-Food_Development/admin/common/list");

		}
	}

	@AfterTest
	public static void close() {
		driver.close();
	}

	@Test(description = "all field empty", groups = "All-Empty", priority = 1)
	public void common_validate_1() {
		try {
			WebElement author = driver.findElement(By.id("title"));
			WebElement submit = driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = getHtml5ValidationMessage(author);
			assertEquals("Please fill out this field.", actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "input field image empty ", groups = "Content", priority = 2)
	public void common_validate_2() {
		try {
			WebElement image_chose = driver.findElement(By.id("image-chose"));
			WebElement submit = driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = getHtml5ValidationMessage(image_chose);
			assertEquals("Please select a file.", actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "input field image correctly ", groups = "Content", priority = 3)
	public void common_validate_3() {
		try {
			WebElement image_chose = driver.findElement(By.id("image-chose"));
			image_chose.sendKeys(data.image);

			WebElement submit = driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = getHtml5ValidationMessage(image_chose);
			assertEquals("", actual);
		} catch (Exception e) {
			System.out.println(e);
			fail("FAILL");
		}
	}

	@Test(description = "input field author empty", groups = "Author", priority = 4)
	public void common_validate_4() {
		try {
			WebElement image_chose = driver.findElement(By.id("image-chose"));
			image_chose.sendKeys(data.image);
			WebElement author = driver.findElement(By.id("author"));
			WebElement submit = driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = getHtml5ValidationMessage(author);
			assertEquals("Please fill out this field.", actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "input field author 19 characters", groups = "Author", priority = 5)
	public void common_validate_5() {
		try {
			WebElement image_chose = driver.findElement(By.id("image-chose"));
			image_chose.sendKeys(data.image);
			WebElement author = driver.findElement(By.id("author"));
			author.sendKeys(data.characters_19);
			WebElement submit = driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = getHtml5ValidationMessage(author);
			assertEquals("Please lengthen this text to 20 characters or more (you are currently using 19 characters).",
					actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "input field author 20 characters", groups = "Author", priority = 6)
	public void common_validate_6() {
		try {
			WebElement image_chose = driver.findElement(By.id("image-chose"));
			image_chose.sendKeys(data.image);
			WebElement author = driver.findElement(By.id("author"));
			author.sendKeys(data.characters_20);
			WebElement submit = driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = getHtml5ValidationMessage(author);
			assertEquals("", actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "input field author  255 characters", groups = "Author", priority = 7)
	public void common_validate_7() {
		try {
			WebElement image_chose = driver.findElement(By.id("image-chose"));
			image_chose.sendKeys(data.image);
			WebElement author = driver.findElement(By.id("author"));
			author.sendKeys(data.characters_255);
			WebElement submit = driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = getHtml5ValidationMessage(author);
			assertEquals("", actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "input field author 256 characters", groups = "Author", priority = 8)
	public void common_validate_8() {
		try {
			WebElement image_chose = driver.findElement(By.id("image-chose"));
			image_chose.sendKeys(data.image);
			WebElement author = driver.findElement(By.id("author"));
			author.sendKeys(data.characters_256);
			WebElement submit = driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = author.getAttribute("value");
			assertEquals(data.characters_255, actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "input field title empty", groups = "Title", priority = 9)
	public void common_validate_9() {
		try {
			WebElement image_chose = driver.findElement(By.id("image-chose"));
			image_chose.sendKeys(data.image);
			WebElement author = driver.findElement(By.id("author"));
			author.sendKeys(data.author);
			WebElement title = driver.findElement(By.id("title"));
			WebElement submit = driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = getHtml5ValidationMessage(title);
			assertEquals("Please fill out this field.", actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "input field title 19 characters", groups = "Title", priority = 10)
	public void common_validate_10() {
		try {
			WebElement image_chose = driver.findElement(By.id("image-chose"));
			image_chose.sendKeys(data.image);
			WebElement author = driver.findElement(By.id("author"));
			author.sendKeys(data.author);
			WebElement title = driver.findElement(By.id("title"));
			title.sendKeys(data.characters_19);
			WebElement submit = driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = getHtml5ValidationMessage(title);
			assertEquals("Please lengthen this text to 20 characters or more (you are currently using 19 characters).",
					actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "input field title 20 characters", groups = "Title", priority = 11)
	public void common_validate_11() {
		try {
			WebElement image_chose = driver.findElement(By.id("image-chose"));
			image_chose.sendKeys(data.image);
			WebElement author = driver.findElement(By.id("author"));
			author.sendKeys(data.author);
			WebElement title = driver.findElement(By.id("title"));
			title.sendKeys(data.characters_20);
			WebElement submit = driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = getHtml5ValidationMessage(title);
			assertEquals("", actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "input field title 100 characters", groups = "Title", priority = 12)
	public void common_validate_12() {
		try {
			WebElement image_chose = driver.findElement(By.id("image-chose"));
			image_chose.sendKeys(data.image);
			WebElement author = driver.findElement(By.id("author"));
			author.sendKeys(data.author);
			WebElement title = driver.findElement(By.id("title"));
			title.sendKeys(data.characters_100);
			WebElement submit = driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = getHtml5ValidationMessage(title);
			assertEquals("", actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "input field title 101 characters", groups = "Title", priority = 13)
	public void common_validate_13() {
		try {
			WebElement image_chose = driver.findElement(By.id("image-chose"));
			image_chose.sendKeys(data.image);
			WebElement author = driver.findElement(By.id("author"));
			author.sendKeys(data.author);
			WebElement title = driver.findElement(By.id("title"));
			title.sendKeys(data.characters_101);
			WebElement submit = driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = title.getAttribute("value");
			assertEquals(data.characters_100, actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "input field content empty", groups = "Content", priority = 14)
	public void common_validate_14() {
		try {
			WebElement image_chose = driver.findElement(By.id("image-chose"));
			image_chose.sendKeys(data.image);
			WebElement author = driver.findElement(By.id("author"));
			author.sendKeys(data.author);
			WebElement title = driver.findElement(By.id("title"));
			title.sendKeys(data.title);
			WebElement content = driver.findElement(By.id("content"));
			WebElement submit = driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = getHtml5ValidationMessage(content);
			assertEquals("Please fill out this field.", actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "input field content 199 characters", groups = "Content", priority = 15)
	public void common_validate_15() {
		try {
			WebElement image_chose = driver.findElement(By.id("image-chose"));
			image_chose.sendKeys(data.image);
			WebElement author = driver.findElement(By.id("author"));
			author.sendKeys(data.author);
			WebElement title = driver.findElement(By.id("title"));
			title.sendKeys(data.title);
			WebElement content = driver.findElement(By.id("content"));
			content.sendKeys(data.characters_199);
			WebElement submit = driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = getHtml5ValidationMessage(content);
			assertEquals(
					"Please lengthen this text to 200 characters or more (you are currently using 199 characters).",
					actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "input field content 200 characters", groups = "Content", priority = 16)
	public void common_validate_16() {
		try {
			WebElement image_chose = driver.findElement(By.id("image-chose"));
			image_chose.sendKeys(data.image);
			WebElement author = driver.findElement(By.id("author"));
			author.sendKeys(data.author);
			WebElement title = driver.findElement(By.id("title"));
			title.sendKeys(data.title);
			WebElement content = driver.findElement(By.id("content"));
			content.sendKeys(data.characters_200);
			WebElement submit = driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			WebElement example = driver.findElement(By.id("example"));
			boolean actual = example.isDisplayed();
			assertTrue(actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "field type selected 'Mẹo hay',field kind is displayed option  of class tip", groups = "selection", priority = 17)
	public void common_validate_17() {
		try {
			Select type = new Select(driver.findElement(By.id("type")));
			type.selectByValue("Mẹo hay");
			WebElement kind = driver.findElement(By.className("tip"));
			boolean actual = kind.isDisplayed();
			assertTrue(actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "field type selected 'Dinh dưỡng',field kind is displayed option  of class nutri", groups = "selection", priority = 18)
	public void common_validate_18() {
		try {
			Select type = new Select(driver.findElement(By.id("type")));
			type.selectByValue("Dinh dưỡng");
			WebElement kind = driver.findElement(By.className("nutri"));
			boolean actual = kind.isDisplayed();
			assertTrue(actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "delete button common, check the success message displayed", groups = "delete common", priority = 19)
	public void common_delete_19() {
		try {

			WebElement delete = driver.findElement(
					By.xpath("/html/body/div/div[1]/div/div[2]/div[3]/div/div/table/tbody/tr[1]/td[6]/button"));
			delete.click();
			Thread.sleep(1000);
			WebElement confirm = driver
					.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[2]/div[2]/div/div/div[3]/a"));
			confirm.click();
			Thread.sleep(1000);
			WebElement message = Common.driver.findElement(By.id("hideMe"));
			String actual = message.getAttribute("textContent").trim();
			assertEquals("Thao tác thành công.", actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "delete button common, check the error message displayed", groups = "delete common", priority = 20)
	public void common_delete_20() {
		try {

			driver.get("http://localhost:8080/FPT-Food_Development/admin/common/delete/0");
			Thread.sleep(2000);

			WebElement message = Common.driver.findElement(By.id("hideMe"));
			String actual = message.getAttribute("textContent").trim();
			assertEquals("Thao tác thất bại.", actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "click icon update, redirect to update common page", groups = "update common", priority = 21)
	public void common_update_21() {
		try {
			String id = driver
					.findElement(By.xpath("/html/body/div/div[1]/div/div[2]/div[3]/div/div/table/tbody/tr/td[1]"))
					.getText();

			WebElement edit = driver
					.findElement(By.xpath("/html/body/div/div[1]/div/div[2]/div[3]/div/div/table/tbody/tr/td[6]/a"));
			edit.click();
			String actual = driver.getCurrentUrl();
			String expected = "http://localhost:8080/FPT-Food_Development/admin/common/edit/" + id;

			assertEquals(expected, actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "click button 'Cập nhập', button worked, updated, message success ", groups = "update common", priority = 22)
	public void common_update_22() {
		try {
			WebElement edit_page = driver
					.findElement(By.xpath("/html/body/div/div[1]/div/div[2]/div[3]/div/div/table/tbody/tr/td[6]/a"));
			edit_page.click();
			WebElement submit = driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(2000);
			WebElement message = driver.findElement(By.id("hideMe"));
			String actual = message.getAttribute("textContent").trim();
			assertEquals("Thao tác thành công.", actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "click button 'Quay lại',button worked, redirect to list common page ", groups = "update common", priority = 23)
	public void common_update_23() {
		try {
			WebElement edit = driver
					.findElement(By.xpath("/html/body/div/div[1]/div/div[2]/div[3]/div/div/table/tbody/tr/td[6]/a"));
			edit.click();
			Thread.sleep(2000);

			WebElement cancel = driver.findElement(By.id("cancel"));
			cancel.click();
			String actual = driver.getCurrentUrl();
			assertEquals("http://localhost:8080/FPT-Food_Development/admin/common/list", actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "list common page ", groups = "list common", priority = 24)
	public void common_list_24() {
		try {
			WebElement table = driver.findElement(By.id("example"));

			boolean actual = table.isEnabled();
			assertTrue(actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "list common page title for table displayed corrent", groups = "layout list", priority = 25)
	public void common_list_25() {
		try {
			WebElement table = driver.findElement(By.xpath("/html/body/div/div[1]/div/div[2]/div[1]/div/h1"));
			String actual = table.getAttribute("textContent").trim();

			assertEquals("Mẹo hay & Dinh dưỡng", actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "list common page pagination is displayed", groups = "layout list", priority = 26)
	public void common_list_26() {
		try {
			WebElement pagination = driver.findElement(By.id("example_paginate"));
			boolean actual = pagination.isEnabled();

			assertTrue(actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "list common page buttons export is displayed", groups = "layout list", priority = 27)
	public void common_list_27() {
		try {
			WebElement pagination = driver.findElement(By.xpath("//*[@id=\"example_wrapper\"]/div[1]"));
			boolean actual = pagination.isEnabled();

			assertTrue(actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "list common page search input is displayed", groups = "layout list", priority = 28)
	public void common_list_28() {
		try {
			WebElement pagination = driver.findElement(By.xpath("//*[@id=\"example_filter\"]"));
			boolean actual = pagination.isEnabled();

			assertTrue(actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "list common page left menu is displayed", groups = "layout list", priority = 29)
	public void common_list_29() {
		try {
			WebElement pagination = driver.findElement(By.xpath("//*[@id=\"default-drawer\"]/div[2]/div\r\n"));
			boolean actual = pagination.isEnabled();

			assertTrue(actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	public String getHtml5ValidationMessage(WebElement element) {
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		return (String) jsExecutor.executeScript("return arguments[0].validationMessage;", element);
	}
}
