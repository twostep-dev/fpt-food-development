package selenium;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class User {

	
	@BeforeMethod
	public static void ListUser() {
		Common.driver.get("http://localhost:8080/FPT-Food_Development/admin/user/list");
	}

	
	@Test(description = "Check current session", groups = "List User", priority = 1)
	public void UserList() {
		try {
			String actual = Common.driver.getCurrentUrl();
			System.out.println("==============URL: " + actual);
			assertEquals("http://localhost:8080/FPT-Food_Development/admin/user/list", actual);
		} catch (Exception e) {
			System.out.println(e);
			fail("FAILL");
		}
	}

	public String getHtml5ValidationMessage(WebElement element) {
		JavascriptExecutor jsExecutor = (JavascriptExecutor) Common.driver;
		return (String) jsExecutor.executeScript("return arguments[0].validationMessage;", element);
	}
}
