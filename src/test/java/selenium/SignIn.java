package selenium;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import connecter.data;

public class SignIn {

	@BeforeMethod
	public static void LoginPage() {
		Common.driver.get("http://localhost:8080/FPT-Food_Development/authorized/SignIn");
	}

	@Test(description = "all fields empty", groups = "Login", priority = 1)
	public void Login_1() {
		try {
			WebElement username = Common.driver.findElement(By.id("username"));
			WebElement submit = Common.driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = getHtml5ValidationMessage(username);
			assertEquals("Please fill out this field.", actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "field username empty ", groups = "Login", priority = 2)
	public void Login_2() {
		try {
			WebElement username = Common.driver.findElement(By.id("username"));
			WebElement password = Common.driver.findElement(By.id("password"));
			password.sendKeys(data.password);
			WebElement submit = Common.driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = getHtml5ValidationMessage(username);
			assertEquals("Please fill out this field.", actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "field password empty ", groups = "Login", priority = 3)
	public void Login_3() {
		try {
			WebElement password = Common.driver.findElement(By.id("password"));
			WebElement username = Common.driver.findElement(By.id("username"));
			username.sendKeys(data.username);
			WebElement submit = Common.driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = getHtml5ValidationMessage(password);
			assertEquals("Please fill out this field.", actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "input field username 9 characters ", groups = "Login", priority = 4)
	public void Login_4() {
		try {
			WebElement username = Common.driver.findElement(By.id("username"));
			username.sendKeys(data.characters_9);
			WebElement submit = Common.driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = getHtml5ValidationMessage(username);
			assertEquals("Please lengthen this text to 10 characters or more (you are currently using 9 characters).",
					actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "input field username 10 characters ", groups = "Login", priority = 5)
	public void Login_5() {
		try {
			WebElement username = Common.driver.findElement(By.id("username"));
			username.sendKeys(data.characters_10);
			WebElement submit = Common.driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = getHtml5ValidationMessage(username);
			assertEquals("", actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "input field username 50 characters ", groups = "Login", priority = 6)
	public void Login_6() {
		try {
			WebElement username = Common.driver.findElement(By.id("username"));
			username.sendKeys(data.characters_50);
			WebElement submit = Common.driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = getHtml5ValidationMessage(username);
			assertEquals("", actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "input field username 51 characters ", groups = "Login", priority = 7)
	public void Login_7() {
		try {
			WebElement username = Common.driver.findElement(By.id("username"));
			username.sendKeys(data.characters_51);
			WebElement submit = Common.driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			String actual = username.getAttribute("value");
			assertEquals(data.characters_50, actual);
		} catch (Exception e) {
			fail("FAILL");
		}
	}

	@Test(description = "username password incorrect", groups = "Login", priority = 100)
	public void Login_100() {
		try {
			WebElement username = Common.driver.findElement(By.id("username"));
			username.sendKeys("username_test");
			WebElement password = Common.driver.findElement(By.id("password"));
			password.sendKeys("password");
			WebElement submit = Common.driver.findElement(By.id("submit"));
			submit.click();
			Thread.sleep(1000);
			WebElement message = Common.driver.findElement(By.id("hideMe"));
			String actual = message.getAttribute("textContent").trim();
			assertEquals("Mật khẩu hoặc tài khoản không chính xác.", actual);
		} catch (Exception e) {
			System.out.println(e);
			fail("FAILL");
		}
	}

	public String getHtml5ValidationMessage(WebElement element) {
		JavascriptExecutor jsExecutor = (JavascriptExecutor) Common.driver;
		return (String) jsExecutor.executeScript("return arguments[0].validationMessage;", element);
	}
}
