package unit;

import static org.testng.Assert.*;
import java.util.Set;
import javax.validation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.*;
import org.springframework.test.context.testng.*;
import org.testng.annotations.*;
import com.fpt.dao.*;
import com.fpt.model.*;
import connecter.data;

@Test
@ContextConfiguration(locations = { "../connecter/spring-servlet.xml" })

public class SignUp extends AbstractTestNGSpringContextTests {

	@Autowired(required = true)
	UserDao userDao;
	Users user;

	private static Validator validator;

	@BeforeMethod
	void beforeMethod() {
		user = new Users();
	}

	@BeforeClass
	public static void setUp() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	@Test(description = "All column empty.", groups = "user validate", priority = 1)
	public void user_validate_1() {
		boolean result = userDao.Create(user);
		assertFalse(result);
	}

}
