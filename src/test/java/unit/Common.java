package unit;

import static org.testng.Assert.*;
import java.util.Set;
import javax.validation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.*;
import org.springframework.test.context.testng.*;
import org.testng.annotations.*;
import com.fpt.dao.*;
import com.fpt.model.*;
import connecter.data;

@Test
@ContextConfiguration(locations = { "../connecter/spring-servlet.xml" })

public class Common extends AbstractTestNGSpringContextTests {

	@Autowired(required = true)
	CommonDao commonDao;
	Commons common;

	@Autowired(required = true)
	UserDao userDao;

	private static Validator validator;

	@BeforeMethod
	void beforeMethod() {
		common = new Commons();
	}

	@BeforeClass
	public static void setUp() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	@Test(description = "All column empty.", groups = "common validate", priority = 1)
	public void common_validate_1() {
		boolean result = commonDao.Create(common);
		
		
		assertFalse(result);
//		assertTrue(result);


	}

//	@Test(description = "Only input column title.", groups = "common validate", priority = 2)
//	public void common_validate_2() {
//		common.setTitle(data.title);
//		boolean result = commonDao.Create(common);
//		assertFalse(result);
//
//	}
//
//	@Test(description = "Only input column content.", groups = "common validate", priority = 3)
//	public void common_validate_3() {
//		common.setContent(data.content);
//		boolean result = commonDao.Create(common);
//		assertFalse(result);
//
//	}
//
//	@Test(description = "Only input column type.", groups = "common validate", priority = 4)
//	public void common_validate_4() {
//		common.setType(data.type);
//		;
//		boolean result = commonDao.Create(common);
//		assertFalse(result);
//
//	}
//
//	@Test(description = "Only input column author.", groups = "common validate", priority = 5)
//	public void common_validate_5() {
//		common.setAuthor(data.author);
//		boolean result = commonDao.Create(common);
//		assertFalse(result);
//
//	}
//
//	@Test(description = "Only input column image.", groups = "common validate", priority = 6)
//	public void common_validate_6() {
//		common.setImage(data.image);
//		boolean result = commonDao.Create(common);
//		assertFalse(result);
//
//	}
//
//	@Test(description = "Only input column user_id.", groups = "common validate", priority = 7)
//	public void common_validate_7() {
//		common.setUser(userDao.all().get(0));
//		boolean result = commonDao.Create(common);
//		assertFalse(result);
//
//	}
//
//	@Test(description = "Leave column image blank and other column input correctly.", groups = "common validate", priority = 8)
//	public void common_validate_8() {
//		boolean result = commonDao.Create(new Commons(data.title, data.content, data.type, data.author, data.created_at,
//				data.kind, null, userDao.all().get(0)));
//		assertFalse(result);
//
//	}
//
//	@Test(description = "Leave column title blank and other column input correctly.", groups = "common validate", priority = 9)
//	public void common_validate_9() {
//		boolean result = commonDao.Create(new Commons(null, data.content, data.type, data.author, data.created_at,
//				data.kind, data.image, userDao.all().get(0)));
//		assertFalse(result);
//
//	}
//
//	@Test(description = "Leave column content blank and other column input correctly.", groups = "common validate", priority = 10)
//	public void common_validate_10() {
//		boolean result = commonDao.Create(new Commons(data.title, null, data.type, data.author, data.created_at,
//				data.kind, data.image, userDao.all().get(0)));
//		assertFalse(result);
//
//	}
//
//	@Test(description = "Leave column type blank and other column input correctly.", groups = "common validate", priority = 11)
//	public void common_validate_11() {
//		boolean result = commonDao.Create(new Commons(data.title, data.content, null, data.author, data.created_at,
//				data.kind, data.image, userDao.all().get(0)));
//		assertFalse(result);
//	}
//
//	@Test(description = "Leave column author blank and other column input correctly.", groups = "common validate", priority = 12)
//	public void common_validate_12() {
//		boolean result = commonDao.Create(new Commons(data.title, data.content, data.type, null, data.created_at,
//				data.kind, data.image, userDao.all().get(0)));
//		assertFalse(result);
//	}
//
//	@Test(description = "Leave column kind blank and other column input correctly.", groups = "common validate", priority = 13)
//	public void common_validate_13() {
//		boolean result = commonDao.Create(new Commons(data.title, data.content, data.type, data.author, data.created_at,
//				null, data.image, userDao.all().get(0)));
//		assertFalse(result);
//	}
//
//	@Test(description = "Leave column image blank and other column input correctly.", groups = "common validate", priority = 14)
//	public void common_validate_14() {
//		boolean result = commonDao.Create(new Commons(data.title, data.content, data.type, data.author, data.created_at,
//				data.kind, null, userDao.all().get(0)));
//		assertFalse(result);
//	}
//
//	@Test(description = "Leave column user blank and other column input correctly.", groups = "common validate", priority = 15)
//	public void common_validate_15() {
//		boolean result = commonDao.Create(new Commons(data.title, data.content, data.type, data.author, data.created_at,
//				data.kind, data.image, null));
//		assertFalse(result);
//	}
//
//	@Test(description = "input column title 19 characters and other column input correctly.", groups = "common validate", priority = 16)
//	public void common_validate_16() {
//		common.setAuthor(data.author);
//		common.setTitle(data.characters_19);
//		common.setContent(data.content);
//		common.setCreated_at(data.created_at);
//		common.setImage(data.image);
//		common.setType(data.type);
//		common.setKind(data.kind);
//		common.setUser(userDao.all().get(0));
//		Set<ConstraintViolation<Commons>> constraintViolations = validator.validate(common);
//		assertEquals("size must be between 20 and 100", constraintViolations.iterator().next().getMessage());
//	}
//
//	@Test(description = "input column title 20 characters and other column input correctly.", groups = "common validate", priority = 17)
//	public void common_validate_17() {
//		boolean result = commonDao.Create(new Commons(data.characters_20, data.content, data.type, data.author,
//				data.created_at, data.kind, data.image, null));
//		assertFalse(result);
//	}
//
//	@Test(description = "input column title 100 characters and other column input correctly.", groups = "common validate", priority = 18)
//	public void common_validate_18() {
//		boolean result = commonDao.Create(new Commons(data.characters_100, data.content, data.type, data.author,
//				data.created_at, data.kind, data.image, null));
//		assertFalse(result);
//	}
//
//	@Test(description = "input column title 101 characters and other column input correctly.", groups = "common validate", priority = 19)
//	public void common_validate_19() {
//		common.setAuthor(data.author);
//		common.setTitle(data.characters_101);
//		common.setContent(data.content);
//		common.setCreated_at(data.created_at);
//		common.setImage(data.image);
//		common.setType(data.type);
//		common.setKind(data.kind);
//		common.setUser(userDao.all().get(0));
//		Set<ConstraintViolation<Commons>> constraintViolations = validator.validate(common);
//		assertEquals("size must be between 20 and 100", constraintViolations.iterator().next().getMessage());
//	}
//
//	@Test(description = "input column content 199 characters and other column input correctly.", groups = "common validate", priority = 20)
//	public void common_validate_20() {
//		common.setAuthor(data.author);
//		common.setTitle(data.title);
//		common.setContent(data.characters_199);
//		common.setCreated_at(data.created_at);
//		common.setImage(data.image);
//		common.setType(data.type);
//		common.setKind(data.kind);
//		common.setUser(userDao.all().get(0));
//		Set<ConstraintViolation<Commons>> constraintViolations = validator.validate(common);
//		assertEquals("size must be between 200 and 2147483647", constraintViolations.iterator().next().getMessage());
//	}
//
//	@Test(description = "input column content 200 characters and other column input correctly.", groups = "common validate", priority = 21)
//	public void common_validate_21() {
//		boolean result = commonDao.Create(new Commons(data.title, data.characters_200, data.type, data.author,
//				data.created_at, data.kind, data.image, null));
//		assertFalse(result);
//	}
//
//	@Test(description = "input column author 19 characters and other column input correctly.", groups = "common validate", priority = 22)
//	public void common_validate_22() {
//		common.setAuthor(data.characters_19);
//		common.setTitle(data.title);
//		common.setContent(data.content);
//		common.setCreated_at(data.created_at);
//		common.setImage(data.image);
//		common.setType(data.type);
//		common.setKind(data.kind);
//		common.setUser(userDao.all().get(0));
//		Set<ConstraintViolation<Commons>> constraintViolations = validator.validate(common);
//		assertEquals("size must be between 20 and 255", constraintViolations.iterator().next().getMessage());
//	}
//
//	@Test(description = "input column author 20 characters and other column input correctly.", groups = "common validate", priority = 23)
//	public void common_validate_23() {
//		boolean result = commonDao.Create(new Commons(data.title, data.content, data.type, data.characters_20,
//				data.created_at, data.kind, data.image, null));
//		assertFalse(result);
//	}
//
//	@Test(description = "input column author 255 characters and other column input correctly.", groups = "common validate", priority = 24)
//	public void common_validate_24() {
//		boolean result = commonDao.Create(new Commons(data.title, data.content, data.type, data.characters_255,
//				data.created_at, data.kind, data.image, null));
//		assertFalse(result);
//	}
//
//	@Test(description = "input column author 256 characters and other column input correctly.", groups = "common validate", priority = 25)
//	public void common_validate_25() {
//		common.setAuthor(data.characters_256);
//		common.setTitle(data.title);
//		common.setContent(data.content);
//		common.setCreated_at(data.created_at);
//		common.setImage(data.image);
//		common.setType(data.type);
//		common.setKind(data.kind);
//		common.setUser(userDao.all().get(0));
//		Set<ConstraintViolation<Commons>> constraintViolations = validator.validate(common);
//		assertEquals("size must be between 20 and 255", constraintViolations.iterator().next().getMessage());
//	}
//
//	@Test(description = "Create common", groups = "create common", priority = 26)
//	public void common_create_26() {
//		boolean result = commonDao.Create(new Commons(data.title, data.content, data.type, data.author, data.created_at,
//				data.kind, data.image, userDao.all().get(0)));
//		assertTrue(result);
//	}
//
//	@Test(description = "Delete record exists.", groups = "delete common", priority = 27)
//	public void common_delete_27() {
//		commonDao.Create(new Commons(data.title, data.content, data.type, data.author, data.created_at, data.kind,
//				data.image, userDao.all().get(0)));
//		boolean result = commonDao.Delete(commonDao.list().get(0).getId());
//		assertTrue(result);
//	}
//
//	@Test(description = "Delete record not exists.", groups = "delete common", priority = 28)
//	public void common_delete_28() {
//		commonDao.Create(new Commons(data.title, data.content, data.type, data.author, data.created_at, data.kind,
//				data.image, userDao.all().get(0)));
//		int Id = (commonDao.list().get(commonDao.list().size() - 1).getId()) + 1;
//		boolean result = commonDao.Delete(Id);
//		assertFalse(result);
//
//	}
//
//	@Test(description = "Get list common.", groups = "list common", priority = 29)
//	public void common_list_29() {
//		commonDao.Create(new Commons(data.title, data.content, data.type, data.author, data.created_at, data.kind,
//				data.image, userDao.all().get(0)));
//		assertNotNull(commonDao.list());
//
//	}
//
//	@Test(description = "Find by ID.", groups = "list common", priority = 30)
//	public void common_findBy_id_30() {
//		assertNotNull(commonDao.Delete(commonDao.list().get(0).getId()));
//
//	}
//
//	@Test(description = "Find by type.", groups = "list common", priority = 31)
//	public void common_findBy_type_31() {
//		assertNotNull(commonDao.filterByType(data.type));
//
//	}
//
//	@Test(description = "Find by type and kind.", groups = "list Common", priority = 32)
//	public void common_findBy_typeOrkind_32() {
//		assertNotNull(commonDao.findByTypeKind(data.type, data.kind, 0, 12));
//
//	}
//
//	@Test(description = "Update common exists.", groups = "Update Common", priority = 33)
//	public void common_update_exists_33() {
//		Commons common = commonDao.list().get(0);
//		common.setAuthor("Author change after update successfully");
//		boolean result = commonDao.Update(common);
//		assertTrue(result);
//
//	}
//
//	@Test(description = "Update common not exists.", groups = "Update Common", priority = 34)
//	public void common_update_not_exists_34() {
//		Commons common = commonDao.findByID(0);
//		boolean result = commonDao.Update(common);
//		assertFalse(result);
//
//	}

}
